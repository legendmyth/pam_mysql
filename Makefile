LIBSRC=     pam_mysql.c
LIBOBJ=     pam_mysql.o
LIBLIB=     pam_mysql.so

MYSQL_CFLAGS=`mysql_config --cflags`
MYSQL_LIBS=`mysql_config --libs`
MYSQL_INCLUDE=`mysql_config --include`

LINK=		-L/usr/lib
LDLIBS=		${LINK} -lpam  -lssl -lcrypto -lpam_misc ${MYSQL_LIBS}
INCLUDE=	-I/usr/include ${MYSQL_INCLUDE}
CFLAGS=		 -fPIC -DPIC -Wall -D_GNU_SOURCE ${INCLUDE}


all: ${LIBLIB}

DISTFILES= pam_mysql.c

${LIBLIB}: ${LIBOBJ}
	${CC} ${CFLAGS} ${INCLUDE} -shared -o $@ ${LIBOBJ} ${LDLIBS}

install:
	cp -f ${LIBLIB} /lib64/security/

clean:
	rm -f ${LIBOBJ} ${LIBLIB} 

