/*
 *  * PAM authentication module for MySQL
 *  *
 *  * pam_mysql: zhangjun <18238896@qq.com>
 *  *
 *  * This work is a dirivative of pam_sqlite3 
 *  * (https://github.com/HormyAJP/pam_sqlite3).
 *  *
 *
 *  This work is Copyright (C) zhangjun
 *  GNU GPL v3
 *  This work is a dirivative of pam_sqlite3, Copyright (C) HormyAJP
 *  pam_sqlite3 is a dirivative of pam_sqlite3.
 *
 *  Different portions of this program are Copyright to the respective
 *  authors of those peices of code; but are all under the terms
 *  of of the GNU General Pulblic License.
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.
 *
 *  */

/* $Id: pam_mysql.c,  2019/05/12 8:39:07 $ */

#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <syslog.h>
#include <ctype.h>
#include <unistd.h>
#if HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#include <time.h>

#ifdef HAVE_OPENSSL_MD5_H
#include <openssl/md5.h>
#endif
#ifdef HAVE_OPENSSL_SHA_H
#include <openssl/sha.h>
#endif

#include <sys/cdefs.h>

#ifdef HAVE_MYSQL_H
#include <mysql.h>
#endif

#include <security/pam_modules.h>
#include <security/pam_appl.h>

#define PAM_OPT_USE_FIRST_PASS      0x04
#define PAM_OPT_TRY_FIRST_PASS      0x08
#define PAM_OPT_ECHO_PASS       0x20

#define PAM_MODULE_NAME  "pam_mysql"

#define SYSLOG(x...)  do {                                          \
                          openlog("pam_mysql", LOG_PID, LOG_AUTH); \
                          syslog(LOG_INFO, ##x);                    \
                          closelog();                               \
                      } while(0)
#define SYSLOGERR(x...) SYSLOG("Error: " x)

#define FAIL(MSG)       \
    {                   \
        SYSLOGERR(MSG); \
        free(buf);      \
        return NULL;    \
    }

#define GROW(x)     if (x > buflen - dest - 1) {            \
    char *grow;                                             \
    buflen += 256 + x;                                      \
    grow = realloc(buf, buflen + 256 + x);                  \
    if (grow == NULL) FAIL("Out of memory building query"); \
    buf = grow;                                             \
}

#define APPEND(str, len)    GROW(len); memcpy(buf + dest, str, len); dest += len
#define APPENDS(str)    len = strlen(str); APPEND(str, len)

/*
 * * Being very defensive here. The current logic in the rest of the code should prevent this from
 * * happening. But lets protect against future code changes which could cause a NULL ptr to creep
 * * in.
 * */
#define CHECK_STRING(str)                                                       \
    if (!str)                                                                   \
        FAIL("Internal error in format_query: string ptr " #str " was NULL");

static const char* UNKNOWN_SERVICE = "<Unknown Service>";

typedef enum {
    PW_CLEAR = 0,
    PW_MD5 = 1,
    PW_SHA = 2,
    PW_SHA256 = 3
} pw_scheme;

/* Options */
struct module_options {
    char *host;
    char *port;
    char *db;
    char *user;
    char *passwd;
    char *table;
    char *usercol;
    char *passwdcol;
    char *expirecol;
    pw_scheme pw_type;
};

static void *
xcalloc(size_t nmemb, size_t size)
{
    void *retval;
    double v = ((double)size) * (int)(nmemb & (((size_t)-1) >> 1));

    if (v != nmemb * size) {
        return NULL;
    }

    retval = calloc(nmemb, size);

    return retval;
}

#ifdef HAVE_OPENSSL_MD5_H
#define HAVE_PAM_MD5_DATA
/* pam_md5_data */
static char *
pam_md5_data(const char *d, unsigned int sz, char *md)
{
    size_t i, j;
    unsigned char buf[16];

    if (md == NULL) {
        if ((md = xcalloc(32 + 1, sizeof(char))) == NULL) {
            return NULL;
        }
    }

    MD5((unsigned char*)d, (unsigned long)sz, buf);

    for (i = 0, j = 0; i < 16; i++, j += 2) {
        md[j + 0] = "0123456789abcdef"[(int)(buf[i] >> 4)];
        md[j + 1] = "0123456789abcdef"[(int)(buf[i] & 0x0f)];
    }
    md[j] = '\0';

    return md;
}
#endif

#ifdef HAVE_OPENSSL_SHA_H
#define HAVE_PAM_SHA_DATA
/* pam_sha_data */
static char *
pam_sha_data(const char *d, unsigned int sz, char *md)
{
    size_t i, j;
    unsigned char buf[SHA_DIGEST_LENGTH];

    if (md == NULL) {
        if ((md = xcalloc(SHA_DIGEST_LENGTH*2 + 1, sizeof(char))) == NULL) {
            return NULL;
        }
    }

    SHA1((unsigned char*)d, (unsigned long)sz, buf);

    for (i = 0, j = 0; i < SHA_DIGEST_LENGTH; i++, j += 2) {
        md[j + 0] = "0123456789abcdef"[(int)(buf[i] >> 4)];
        md[j + 1] = "0123456789abcdef"[(int)(buf[i] & 0x0f)];
    }
    md[j] = '\0';

    return md;
}

static char *
pam_sha256_data(const char *d, unsigned int sz, char *md)
{
    size_t i, j;
    unsigned char buf[SHA_DIGEST_LENGTH];

    if (md == NULL) {
        if ((md = xcalloc(SHA_DIGEST_LENGTH*2 + 1, sizeof(char))) == NULL) {
            return NULL;
        }
    }

    SHA256((unsigned char*)d, (unsigned long)sz, buf);

    for (i = 0, j = 0; i < SHA_DIGEST_LENGTH; i++, j += 2) {
        md[j + 0] = "0123456789abcdef"[(int)(buf[i] >> 4)];
        md[j + 1] = "0123456789abcdef"[(int)(buf[i] & 0x0f)];
    }
    md[j] = '\0';

    return md;
}
#endif

static char *
format_query(MYSQL *mysql, const char *template, struct module_options *options,
                          const char *user, const char *passwd)
{
    char *buf = malloc(256);
    if (!buf)
        return NULL;

    int buflen = 256;
    int dest = 0, len;
    const char *src = template;
    char *pct;
    char *tmp;

    while (*src) {
        pct = strchr(src, '%');

        if (pct) {
            /* copy from current position to % char into buffer */
            if (pct != src) {
                len = pct - src;
                APPEND(src, len);
            }

            /* decode the escape */
            switch(pct[1]) {
            case 'U':   /* mysql user */
                if (user) {
                    tmp = xcalloc(strlen(user), sizeof(char));
                    long tmplen = mysql_real_escape_string(mysql, tmp, user, strlen(user));
                    if (tmplen == -1)
                        FAIL("Illegal character exists in the user field");
                    len = strlen(tmp);
                    APPEND(tmp, len);
                    free(tmp);
                }
                break;
            case 'P':   /* mysql passwd */
                if (passwd) {
                    tmp = xcalloc(strlen(passwd), sizeof(char));
                    long tmplen = mysql_real_escape_string(mysql, tmp, passwd, strlen(passwd));
                    if (tmplen == -1)
                        FAIL("Illegal character exists in the passwd field");
                    len = strlen(tmp);
                    APPEND(tmp, len);
                    free(tmp);
                }
                break;
            case 'O':   /* option value */
                pct++;
                switch (pct[1]) {
                case 't':   /* table */
                    CHECK_STRING(options->table);
                    APPENDS(options->table);
                    break;
                case 'u':   /* username */
                    CHECK_STRING(options->usercol);
                    APPENDS(options->usercol);
                    break;
                case 'p':   /* password */
                    CHECK_STRING(options->passwdcol);
                    APPENDS(options->passwdcol);
                    break;
                case 'e':  /* expire */
                    CHECK_STRING(options->expirecol);
                    APPENDS(options->expirecol);
                    break;
                }
                break;

            case '%':   /* quoted % sign */
                APPEND(pct, 1);
                break;

            default:    /* unknown */
                APPEND(pct, 2);
                break;
            }
            src = pct + 2;
        } else {
            /* copy rest of string into buffer and we're done */
            len = strlen(src);
            APPEND(src, len);
            break;
        }
    }

    buf[dest] = '\0';
    return buf;
}

/*
 *  * safe_assign protects against duplicate config options causing a memory leak.
 *  */
static void inline
safe_assign(char **asignee, const char *val)
{
    if(*asignee)
        free(*asignee);
    *asignee = strdup(val);
}

static int
pam_conv_pass(pam_handle_t *pamh, int options)
{
    int retval;
    const void *item;
    const struct pam_conv *conv;
    struct pam_message msg;
    const struct pam_message *msgs[1];
    struct pam_response *resp;

    if ((retval = pam_get_item(pamh, PAM_CONV, &item)) != PAM_SUCCESS)
        return retval;
    conv = (const struct pam_conv *)item;
    msg.msg_style = options & PAM_OPT_ECHO_PASS ? PAM_PROMPT_ECHO_ON : PAM_PROMPT_ECHO_OFF;
    msg.msg = "";
    msgs[0] = &msg;
    if ((retval = conv->conv(1, msgs, &resp, conv->appdata_ptr)) != PAM_SUCCESS)
        return retval;
    if ((retval = pam_set_item(pamh, PAM_AUTHTOK, resp[0].resp)) != PAM_SUCCESS)
        return retval;
    memset(resp[0].resp, 0, strlen(resp[0].resp));
    free(resp[0].resp);
    free(resp);
    return PAM_SUCCESS;
}

int
pam_get_pass(pam_handle_t *pamh, const char **passp, int options)
{
    int retval;
    const void *item = NULL;

    /*
     * Grab the already-entered password if we might want to use it.
     */
    if (options & (PAM_OPT_TRY_FIRST_PASS | PAM_OPT_USE_FIRST_PASS)) {
        if ((retval = pam_get_item(pamh, PAM_AUTHTOK, &item)) != PAM_SUCCESS)
            return retval;
    }

    if (item == NULL) {
        /* The user hasn't entered a password yet. */
        if (options & PAM_OPT_USE_FIRST_PASS)
            return PAM_AUTH_ERR;
        /* Use the conversation function to get a password. */
        if ((retval = pam_conv_pass(pamh, options)) != PAM_SUCCESS ||
                (retval = pam_get_item(pamh, PAM_AUTHTOK, &item)) != PAM_SUCCESS)
            return retval;
    }
    *passp = (const char *)item;
    return PAM_SUCCESS;
}

const char*
pam_get_service(pam_handle_t *pamh, const char **service)
{
    if (pam_get_item(pamh, PAM_SERVICE, (const void**)service) != PAM_SUCCESS)
        *service = UNKNOWN_SERVICE;
    return *service;
}

/* private: parse and set the specified string option */
static void
set_module_option(const char *option, struct module_options *options)
{
    char *buf, *eq;
    char *val, *end;

    if(!option || !*option)
        return;

    buf = strdup(option);
    if(!buf)
        return;

    if((eq = strchr(buf, '='))) {
        end = eq - 1;
        val = eq + 1;
        if(end <= buf || !*val) {
            free(buf);
            return;
        }
        while(end > buf && isspace(*end))
            end--;
        end++;
        *end = '\0';
        while(*val && isspace(*val))
            val++;
    } else {
        val = NULL;
    }

    // SYSLOG("setting option: %s=>%s\n", buf, val);
    if(!strcmp(buf, "host")) {
        safe_assign(&options->host, val);
    } else if(!strcmp(buf, "port")) {
        safe_assign(&options->port, val);
    } else if(!strcmp(buf, "db")) {
        safe_assign(&options->db, val);
    } else if(!strcmp(buf, "user")) {
        safe_assign(&options->user, val);
    } else if(!strcmp(buf, "passwd")) {
        safe_assign(&options->passwd, val);
    } else if(!strcmp(buf, "table")) {
        safe_assign(&options->table, val);
    } else if(!strcmp(buf, "usercol")) {
        safe_assign(&options->usercol, val);
    } else if(!strcmp(buf, "passwdcol")) {
        safe_assign(&options->passwdcol, val);
    } else if(!strcmp(buf, "expirecol")) {
        safe_assign(&options->expirecol, val);
    } else if(!strcmp(buf, "crypt")) {
        if(!strcmp(val, "1")) {
            options->pw_type = PW_MD5;
        } else if(!strcmp(val, "2")) {
            options->pw_type = PW_SHA;
        } else if(!strcmp(val, "3")) {
            options->pw_type = PW_SHA256;
        } else {
            options->pw_type = PW_CLEAR;
        }
    } else {
        SYSLOG("ignored option: %s\n", buf);
    }

    free(buf);
}

/* private: read module options from commandline */
static int
get_module_options(int argc, const char **argv, struct module_options **options)
{
    int i, rc;
    struct module_options *opts;

    rc = 0;
    if (!(opts = (struct module_options *)malloc(sizeof *opts))) {
        *options = NULL;
        return rc;
    }

    bzero(opts, sizeof(*opts));
    opts->pw_type = PW_CLEAR;

    for(i = 0; i < argc; i++) {
        set_module_option(argv[i], opts);
    }
    *options = opts;

    return rc;
}

/* private: free module options returned by get_module_options() */
static void
free_module_options(struct module_options *options)
{
    if (!options)
        return;

    if(options->host)
        free(options->host);
    if(options->port)
        free(options->port);
    if(options->db)
        free(options->db);
    if(options->user)
        free(options->user);
    if(options->passwd)
        free(options->passwd);
    if(options->table)
        free(options->table);
    if(options->usercol)
        free(options->usercol);
    if(options->passwdcol)
        free(options->passwdcol);
    if(options->expirecol)
        free(options->expirecol);

    bzero(options, sizeof(*options));
    free(options);
}

/* private: make sure required options are present in cmdline */
static int
options_valid(struct module_options *options)
{
    if(!options) {
        SYSLOGERR("failed to read options.");
        return -1;
    }

    if(options->db == 0 || options->db == NULL || options->table == 0 
        || options->table == NULL || options->user == 0 || options->user == NULL) {
        SYSLOGERR("the database, table and user options are required.");
        return -1;
    }
    return 0;
}

/* private: connect mysql database */
int
pam_mysql_connect(MYSQL *mysql, struct module_options *options)
{
    int port;
    mysql_init(mysql);
    mysql_set_character_set(mysql, "utf8");

    if (options->port == 0 || options->port == NULL) {
        port = 3306;
    } else {
        port = atoi(options->port);
    }
    /*   
     * 1. On Unix, the client uses a Unix socket file when connecting to the ‘localhost' database;
     * 2. the client uses the address and port when connecting to the database via TCP;
     * 3. When the parameter 'host' is NULL, the database is connected by default with 'localhost'
     */
    // mysql->port = port;
    if (mysql_real_connect(mysql, options->host, options->user, options->passwd, options->db, port, NULL, 0) == NULL) {
        SYSLOGERR("%s", mysql_error(mysql));
        return -1;
    }
    return 0;
}

MYSQL_ROW
pam_mysql_query(MYSQL *mysql, const char* query)
{
    MYSQL_RES *res = NULL;
    MYSQL_ROW row = NULL;
     
    if ( mysql_real_query(mysql, query, strlen(query)) != 0 ) {
        SYSLOG("Query Error: %s", mysql_error(mysql));
        return NULL;
    };
    
    if ((res = mysql_store_result(mysql)) == NULL) {
        SYSLOG("Store Error: %s", mysql_error(mysql));
        return NULL;
    }

    if ((row = mysql_fetch_row(res)) == NULL) {
        SYSLOG("Fetch Error: %s", mysql_error(mysql));
        return NULL;
    }

    mysql_free_result(res);
    return row;
}


/* private: authenticate expire for acount  */
static int
auth_verify_expire(const char *user, struct module_options *options)
{
    int rc = PAM_CRED_EXPIRED;
    MYSQL *mysql = malloc(sizeof(MYSQL));
    MYSQL_ROW row = NULL;
    char *query = NULL;

    time_t timenow = {0}, endtime = {0};
    struct tm *timeinfo;

    if( pam_mysql_connect(mysql, options) !=0 ) {
        rc = PAM_AUTH_ERR;
        goto done;
    }

    query = format_query(mysql, "SELECT %Oe FROM %Ot WHERE %Ou='%U'", options, user, NULL);

    row = pam_mysql_query(mysql, query);
    free(query);
    if ( row == NULL || row[0] == NULL) {
        SYSLOGERR("expire query returns NULL");
        rc = PAM_AUTH_ERR;
        goto done;
    }

    const char *expire = row[0];

    char year[5], mon[3], day[3], ym[8];

    char *dtmp = strrchr(expire, '-');
    strncpy(ym, expire, strlen(expire) - strlen(dtmp));
    char *mtmp = strrchr(ym, '-');
    strncpy(year, ym, strlen(ym) - strlen(mtmp));
    strcpy(day, ++dtmp);
    strcpy(mon, ++mtmp);

    timenow = time(NULL);
    timeinfo = localtime(&timenow);
    timeinfo->tm_year = atoi(year) - 1900;
    timeinfo->tm_mon = atoi(mon) - 1;
    timeinfo->tm_mday = atoi(day);
    endtime = mktime(timeinfo);

    if (difftime(endtime, timenow) > 0 ) {
        rc = PAM_SUCCESS;
    }

done:
    mysql_close(mysql);
    free(mysql);
    return rc;
}

/* private: authenticate user and passwd against database */
static int
auth_verify_password(const char *user, const char *passwd, struct module_options *options)
{
    int rc = PAM_AUTH_ERR;
    MYSQL *mysql = malloc(sizeof(MYSQL));
    MYSQL_ROW row = NULL;
    char *query = NULL;
    char *encrypted_pw = NULL;

    if( pam_mysql_connect(mysql, options) !=0 ) {
        rc = PAM_AUTH_ERR;
        goto done;
    }

    query = format_query(mysql, "SELECT %Op FROM %Ot WHERE %Ou='%U'", options, user, NULL);

    row = pam_mysql_query(mysql, query);
    free(query);
    if ( row == NULL || row[0] == NULL ) {
        SYSLOGERR("password query returns NULL");
        rc = PAM_AUTH_ERR;
        goto done;
    }

    const char *stored_pw = row[0];

    switch(options->pw_type) {
    case PW_CLEAR:
        if(strcmp(passwd, stored_pw) == 0)
            rc = PAM_SUCCESS;
        break;
#ifdef HAVE_PAM_MD5_DATA
    case PW_MD5:
        encrypted_pw = pam_md5_data(passwd, strlen(passwd), encrypted_pw);
        if (!encrypted_pw) {
            SYSLOGERR("crypt failed when encrypting password");
            rc = PAM_AUTH_ERR;
            goto done;
        }

        if(strcmp(encrypted_pw, stored_pw) == 0)
            rc = PAM_SUCCESS;
        break;
#endif
#ifdef HAVE_PAM_SHA_DATA
    case PW_SHA:
        encrypted_pw = pam_sha_data(passwd, strlen(passwd), encrypted_pw);
        if (!encrypted_pw) {
            SYSLOGERR("crypt failed when encrypting password");
            rc = PAM_AUTH_ERR;
            goto done;
        }

        if(strcmp(encrypted_pw, stored_pw) == 0)
            rc = PAM_SUCCESS;
        break;
    case PW_SHA256:
        encrypted_pw = pam_sha256_data(passwd, strlen(passwd), encrypted_pw);
        if (!encrypted_pw) {
            SYSLOGERR("crypt failed when encrypting password");
            rc = PAM_AUTH_ERR;
            goto done;
        }

        if(strcmp(encrypted_pw, stored_pw) == 0)
            rc = PAM_SUCCESS;
        break;
    }

#endif

done:
    mysql_close(mysql);
    free(mysql);
    return rc;
}

/* public: authenticate user */
PAM_EXTERN int
pam_sm_authenticate(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    struct module_options *options = NULL;
    const char *user = NULL, *password = NULL, *service = NULL;
    int rc, std_flags;

    std_flags = get_module_options(argc, argv, &options);
    if(options_valid(options) != 0) {
        rc = PAM_AUTH_ERR;
        goto done;
    }

    if((rc = pam_get_user(pamh, &user, NULL)) != PAM_SUCCESS) {
        SYSLOGERR("failed to get username from pam");
        goto done;
    }

    SYSLOG("attempting to authenticate: '%s'", user);

    if((rc = pam_get_pass(pamh, &password, std_flags) != PAM_SUCCESS)) {
        SYSLOGERR("failed to get password from pam");
        goto done;
    }

    if((rc = auth_verify_password(user, password, options)) != PAM_SUCCESS)
        SYSLOG("(%s) user '%s' is not authenticated.", pam_get_service(pamh, &service), user);
    else
        SYSLOG("(%s) user '%s' is authenticated.", pam_get_service(pamh, &service), user);

done:
    free_module_options(options);
    return rc;
}

/* public: check if account has active */
PAM_EXTERN int
pam_sm_acct_mgmt(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    int rc = PAM_AUTH_ERR;
    struct module_options *options = NULL;
    const char *user = NULL, *service = NULL;

    MYSQL *mysql = malloc(sizeof(MYSQL));
    MYSQL_ROW row = NULL;
    char *query = NULL;

    get_module_options(argc, argv, &options);
    if(options_valid(options) != 0) {
        rc = PAM_AUTH_ERR;
        goto done;
    }

    if((rc = pam_get_user(pamh, &user, NULL)) != PAM_SUCCESS) {
        SYSLOGERR("could not retrieve user");
        rc = PAM_AUTH_ERR;
        goto done;
    }

    if( pam_mysql_connect(mysql, options) !=0 ) {
        rc = PAM_AUTH_ERR;
        goto done;
    }

    query = format_query(mysql, "SELECT active from %Ot WHERE %Ou='%U'", options, user, NULL);

    row = pam_mysql_query(mysql, query);
    free(query);
    if ( row == NULL || row[0] == NULL ) {
        rc = PAM_AUTH_ERR;
        goto done;
    }
    
    const char *active = row[0];

    if (atoi(active) == 1) {
        if((rc = auth_verify_expire(user, options)) != PAM_SUCCESS) {
            SYSLOG("(%s) user '%s' has expired.", pam_get_service(pamh, &service), user);
            rc = PAM_ACCT_EXPIRED;
            goto done;
        }
    } else {
        SYSLOG("(%s) user '%s' is not activated.", pam_get_service(pamh, &service), user);
        rc = PAM_ACCT_EXPIRED;
        goto done;
    }

    SYSLOG("(%s) user '%s' is activated.", pam_get_service(pamh, &service), user);
    rc = PAM_SUCCESS;

done:
    mysql_close(mysql);
    free(mysql);
    free_module_options(options);
    return rc;
}

/* public: change password */
PAM_EXTERN int
pam_sm_chauthtok(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    return PAM_SUCCESS;
}

/* public: just succeed. */
PAM_EXTERN int
pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const char **argv)
{
    return PAM_SUCCESS;
}
