# pam_mysql

#### 说明

此模块提供对针对OpenVPN利用MySQL进行身份验证的支持。


#### 安装

1. \# cd pam_mysql
2. \# make && make install

#### 设置

1. 创建服务认证

```
# /etc/pam.d/openvpn
# crypt:  
# 0 = No encryption  
# 1 = md5  
# 2 = sha1
# 3 = sha256 
auth     required pam_mysql.so host=localhost port=3306 db=openvpn user=root passwd=123456 table=t_user usercol=username passwdcol=password expirecol=expire crypt=1
account  required pam_mysql.so host=localhost port=3306 db=openvpn user=root passwd=123456 table=t_user usercol=username passwdcol=password expirecol=expire crypt=1
```
    注：当使用host为'localhost'和port为'3306'连接数据库时，可以将这两个参数省略。

2. 创建openvpn数据库与表

```
create database `openvpn` character set 'utf8' collate 'utf8_general_ci';
create table `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `active` int(11) DEFAULT '1',
  `expire` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
```
